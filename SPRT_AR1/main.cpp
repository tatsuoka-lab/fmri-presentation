
/*
 * This code is customized by Weicong and Curtis to show a module to
 * handle the weight lifting part and computes the
 * formula 3 in paper "Dynamic adjustment of stimuli in real time functional
 * magnetic resonance imaging"
 */
#include "numerical.h"
#include <unistd.h> // usleep()
#include <iomanip>  // std::setw
using namespace std;

typedef cilk::reducer< cilk::op_add<int> > Int_Reducer;

extern int SCAN_SIZE;
extern int DIMENSION_SIZE;

int main(int argc, char* argv[]) {
  if(argc != 3){
    cout << "Usage: " << argv[0] << " Scan_Size Dimension_Size" << endl;
    exit(1);
  }
  SCAN_SIZE = atoi(argv[1]);
  DIMENSION_SIZE = atoi(argv[2]);
  
  int row, col, K_blk;
  double Z; // Z scores to use
  vector<int> scans_to_generate_test_output;
  int temp; // store each scan number to compute Z
  row = SCAN_SIZE;
  col = DIMENSION_SIZE;
  /* the value of the second column is always 1 because this is the response value for each voxel */
  int second_col = 1;
  
  int num_of_C;
  double **C; // C is a selector and actually has the same length with B
  double theta_0, theta_1;
  bool use_pre_assigned_theta1 = false;
  vector<vector<double> > theta_1_C;
  double alpha, beta; // alpha and beta are the stoping rule's parameters
  double crossed_easy_percent, crossed_hard_percent;
  char flag;
  char load_default;

  /*
   * update Mar 1 2019, subregion file has two versions, one is transposed, which is used in
   * real subject experiment, the other is non-transposed, which is used in simulated data
   * all test output files will adjust its orientation accordingly for ease of analysis
   * PS: BOLD output are transposed compared to original nifti file layout.
   */
  bool transpose_subregion_file = true;
  char t_s_f; // user input of whether to confirm transposing

  cout << "Load default setting (y/N)? " << endl;
  cin >> load_default;
  if(toupper(load_default) != 'Y'){
    cout << "Please input parameter K_block: (enter 0 if you wish to set static \u03D1 1)";
    cin >> K_blk;
    cout << "Please input parameter \u03D1 0:";
    cin >> theta_0;
    if(K_blk == 0){
      use_pre_assigned_theta1 = true;
      cout << "Please input parameter \u03D1 1:";
      cin >> theta_1;
    }

    cout << "Please input how many selectors are needed ";
    cin >> num_of_C;
    if(num_of_C < 2){
      cout << "Warning: You can not have less than 2 selectors, otherwise the program will crash. Default set to 2 selctors." << endl;
      num_of_C = 2;
    }
    C = new double*[num_of_C];

    for(int i=0; i<num_of_C; i++){
      C[i] = new double[col];
      cout << "Please input C" << i+1 << " (please note that C1 has the same length of B):";
      for (int j = 0; j < col; j++) 
        cin >> C[i][j];
    }
    
    cout << "Please input alpha and beta values:";
    cin >> alpha;
    cin >> beta;

    cout << "Please input Z score:";
    cin >> Z;

    cout << "Please input which scan(s) would be used to calculate Z score:";
    
    do{
      cin >> temp;
    scans_to_generate_test_output.push_back(temp);
    }while(cin.peek() != '\n');

    cout << "Please input percentage of crossed_easy and crossed_hard: ";
    cin >> crossed_easy_percent;
    cin >> crossed_hard_percent;

    cout << "Transpose subregion file (y/N)?";
    cin >> t_s_f;
    if(toupper(t_s_f) != 'Y') transpose_subregion_file = false;
  }
  else{ // below is the default setting for quick testing

    num_of_C = 4;
    K_blk = 78;
    theta_0 = 0;
    alpha = 0.1;
    beta = 0.01;
    Z = 3.12;
    scans_to_generate_test_output.push_back(100);
    scans_to_generate_test_output.push_back(200);
    crossed_easy_percent = 60;
    crossed_hard_percent = 60;
    C = new double*[num_of_C];
    C[0] = new double[col]{0, 1, 0, 0, 0, 0, 0, 0};
    C[1] = new double[col]{0, 0, 1, 0, 0, 0, 0, 0};
    C[2] = new double[col]{0, 1, -1, 0, 0, 0, 0, 0};
    C[3] = new double[col]{0, -1, 1, 0, 0, 0, 0, 0};
  }

  /* double check the parameters */
  cout << "So the parameters are: " << endl;
  cout << "    " << "K_block is " << K_blk << endl;
  cout << "    " << "\u03D1 0 is " << theta_0 << endl;
  //cout << "    " << "\u03D1 1 is " << theta_1 << endl;
  cout << "    " << "alpha is " << alpha << endl;
  cout << "    " << "beta is " << beta << endl;
  cout << "    " << "Z score is " << Z << endl;
  cout << "    " << "Scans used to compute Z are: ";
  for(vector<int>::const_iterator i = scans_to_generate_test_output.begin(); i != scans_to_generate_test_output.end(); i++ )
    cout << *i << " ";
  cout << endl;
  cout << "    " << "crossed_easy_percent: " << crossed_easy_percent << "%" << endl;
  cout << "    " << "crossed_hard_percent: " << crossed_hard_percent << "%" << endl;
  crossed_easy_percent /= 100.0; // percentage to decimal
  crossed_hard_percent /= 100.0;

  for(int i=0; i<num_of_C; i++){
    cout << "    " << "C" << i+1 << " is [";
    for (int j = 0; j < col; j++) 
      cout << C[i][j] << " ";
    cout << "]" << endl;
  }
  if(transpose_subregion_file) cout << "    " << "Subregion file will be transposed." << endl;
  else cout << "    " << "Subregion file will not be transposed." << endl;

  cout << "Please enter y/n to continue or redo the input: ";
  cin >> flag;
  if (toupper(flag) != 'Y') {
    exit(1);
  }

  vector<double> boundary = stop_boundary(alpha, beta);
  //cout << boundary[0] << endl << boundary[1] << endl;
  if (boundary.size() != 2) {
    throw "Boundaries for stop rules are not correct.";
  }
  
  string file_path_x =
  "./Latest_data/design_easy.txt";
  
  /* resonse matrix needs to be computed iteratively reading docs */
  string file_path_y =
  "./Latest_data/";

  vector<vector<double> > response;
  vector<int> dimensions;

  /* create cout ostream reducer to output results in order */
  cilk::reducer<cilk::op_ostream> cout_r(cout);

  ofstream myfile1;
  ofstream myfile2;
  ofstream myfile3; // used to output _beta_hat value for testing
  myfile1.open("SPRT_statistics.csv"); // stores computation and activation summary
  myfile2.open(file_path_y+"Activation.csv"); // stores activation details used for trigger pulse
  myfile2 << "Scan number,";
  for(int i = 1; i <= num_of_C; i++) myfile2 << "Contrast " << i << " Easy, Contrast" << i << " Hard,";
  myfile2 << endl;

  myfile1 << "alpha: " << alpha << " beta: " << beta <<" lower bound: " << min(boundary[0], boundary[1]) << " upper bound: " << max(boundary[0], boundary[1]) << ",";
  for(int i=0; i<num_of_C; i++){
      for(int j = 0; j < col; j++){
        myfile1 << C[i][j];
      }
      myfile1 << ",,";
  }
  myfile1 << endl;
  myfile1 << "Scan number,";
  for(int i=1; i<=num_of_C; i++){
    myfile1 << "C" << i << " cross upper, C" << i <<" cross lower,";
  }
  myfile1 << "Speed (sec)" << endl;

  double *X;

  /* Creat folder to store test output */
  mkdir("test_files", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
  for(int i = 1; i <= row; i++)
    mkdir(("test_files/" + to_string(i)).c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);

  /*
   * we would like to collect the first scan here 
   * in order to retrive voxel size information
   * This information is used to determine the ROI region
   * knowing the ROI region can boost SPRT's analysis speed
   * 
   * HERE ASSUME ROI REMAINS THE SAME ACROSS ALL SCANS
   */

  cout << "Waiting for the first scan to arrive" << endl;
  // Wait for second scan arrived to avoid synchronization issue. (first scan is bold0.txt)
  while(!check_file_existence(file_path_y+"bold1.txt")){
    // busy wait
  }
  cout << "Caught the first scan, construct data structure..." << endl;

  /* now we have got dimension information stored in dimensions variable. */
  read_scan(response, file_path_y, dimensions, 0);

  vector<double> sigma_hat_square_vector(dimensions[0] * dimensions[1] * dimensions[2], 0.0); 
  /* now we have got "useful" voxel number which is been within ROI */
  int ROI_voxel_number = 0; // all masked voxels
  vector<bool> is_within_ROI(response[0].size(), false);
  for(int i = 0; i < response[0].size(); i++){
    if(response[0][i] != 0.000000){
      ROI_voxel_number++;
      is_within_ROI[i] = true;
    }
  }
  cout << "ROI map constructed! Total ROI voxel number: " << ROI_voxel_number << endl;

  /*
   * WLS analysis variable initialization and declaration
   * This is needed in computing beta_hat and estimate theta_1
   */
  vector<int> subregion;
  unordered_map<int, int> subregion_count;
  int subregion_number = 0;
  cout << "Looking for subregion file ...";
  read_subregion_file("subregion.txt", dimensions, subregion, subregion_number, subregion_count, transpose_subregion_file);
  cout << " Found!" << endl;
  cout << "There are " << subregion_number  << " subregions registered." << endl;
  int subregion_total = 0;
  for(int i = 1; i < subregion_number+1; i++){
    cout << "subregion " << i << ": " << subregion_count[i] << endl;
    subregion_total += subregion_count[i];
  }

  
  int c1 = 0, c2 = 0, c3 = 0, c4 = 0;
  for(int i = 0; i < dimensions[0] * dimensions[1] * dimensions[2]; i++){
    if(subregion[i] == 1 && !is_within_ROI[i]) c1++;
    if(subregion[i] == 2 && !is_within_ROI[i]) c2++;
    if(subregion[i] == 3 && !is_within_ROI[i]) c3++;
    if(subregion[i] == 4 && !is_within_ROI[i]) c4++;
  }
  cout << "There are " << c1 << " voxels inside subregion 1 but has zero value" << endl;
  cout << "There are " << c2 << " voxels inside subregion 2 but has zero value" << endl;
  cout << "There are " << c3 << " voxels inside subregion 3 but has zero value" << endl;
  cout << "There are " << c4 << " voxels inside subregion 4 but has zero value" << endl;
  
  double** W_matrices;
  double** cTXTWX_inverse_c_matrices;
  double* cTXTWX_inverse_c_vector = new double[subregion_number+1]; // used for store cTXTWX_inverse_c s for a specific c value

  double* XTX_inverse = new double[col * col]; // will be used to compute beta_hat
  vector<double> rho_map(dimensions[0] * dimensions[1] * dimensions[2], 0.0);

  double **beta_hat = new double*[dimensions[0] * dimensions[1] * dimensions[2]];
  for(int i = 0; i < dimensions[0] * dimensions[1] * dimensions[2]; i++){
    beta_hat[i] = new double[col * second_col]();
  }
  /*
   * End WLS variable initialization and declaration
   */

  if(!use_pre_assigned_theta1){
    X = new double[K_blk * col];
    init_mat(X, K_blk, col, file_path_x);
    compute_XTX_inverse(XTX_inverse, X, K_blk, col);
    /*
     * Here we we collect first K_blk blocks of scans to dynamically
     * Compute theta_1
     */
    for(int scan_number = 2; scan_number <= K_blk; scan_number++){ // since we have collected the first scan, we start from the second scan
      /* Check existence of file */
        cout << "Waiting for bold" + to_string(scan_number-1)+".txt..." << endl;
        if(scan_number != row){
          while(!check_file_existence(file_path_y+"bold"+to_string(scan_number)+".txt")){
          // busy wait here
          }
        }
        else{
          while(!check_file_existence(file_path_y+"bold"+to_string(scan_number-1)+".txt")){
          /* busy wait here */
          }
        usleep(1000); // wait 1000 milli seconds
      }
      cout << "File arrived. start reading in..." << endl;    
      /* 2-D array response holds the response values for all data points for all time slots */
      read_scan(response, file_path_y, dimensions, scan_number-1);
    }

    cout << "Finish Collecting first " << K_blk << " Scans." << endl;
    cout << "Preparing matierials to compute W matrices" << endl;

    cilk_for (int x = 0; x < dimensions[0]; x++) {
      for (int y = 0; y < dimensions[1]; y++) {
        for (int z = 0; z < dimensions[2]; z++) {
          int pos = x * dimensions[1] * dimensions[2] + y * dimensions[2] + z;

          double *Y = new double[K_blk * second_col]; // Initialize the response matrix
          // response array Y
          for (int i = 0; i < K_blk; i++) Y[i] = response[i][pos];
          compute_beta_hat(X, Y, beta_hat[pos], XTX_inverse, K_blk, col, second_col);
          sigma_hat_square_vector[pos] = estimate_sigma_hat_square(X, Y, beta_hat[pos], K_blk, col, second_col);
          if(is_within_ROI[pos] && subregion[pos] != 0){
            rho_map[pos] = compute_rho(Y, beta_hat[pos], X, sigma_hat_square_vector[pos], K_blk, col, second_col, K_blk);
            //cout << rho_map[pos] << endl;
          }
          delete[] Y;
        }
      }
    }
    clear_2d(beta_hat, dimensions[0] * dimensions[1] * dimensions[2], col * second_col);

    W_matrices = new double* [subregion_number+1];
    cTXTWX_inverse_c_matrices = new double*[subregion_number+1];
    for(int i = 0; i < subregion_number+1; i++){
      W_matrices[i] = new double[K_blk*K_blk];
      cTXTWX_inverse_c_matrices[i] =  new double[num_of_C];
    }
    generate_W_related_matrices(W_matrices, cTXTWX_inverse_c_matrices, X, dimensions, rho_map,
                                  response, is_within_ROI, K_blk, col, second_col, subregion_number, 
                                  subregion_count, subregion, C, num_of_C);
    for(int i=0; i<num_of_C; i++){
      cout << "Start Computing theta_1 For C" << i+1 << endl;
      // construct cTXTWX_inverse_c_vector for every c
      for(int j = 0; j < subregion_number+1; j++) cTXTWX_inverse_c_vector[j] = cTXTWX_inverse_c_matrices[j][i];
      
      theta_1_C.push_back(estimate_theta1(beta_hat, response, dimensions, C[i], K_blk, col, second_col, Z, W_matrices, 
                      cTXTWX_inverse_c_vector, is_within_ROI, subregion, sigma_hat_square_vector, true));

      cout << "theta_1 For C" << i+1 << " is Computed" << endl;
    }
    cout << "Finished Computing All theta_1s At Voxel level." << endl << "Start SPRT Analysis Using the computed theta_1s." << endl;
    delete[] X;
    for(int i = 0; i < subregion_number+1; i++){
        delete[] W_matrices[i];
        delete[] cTXTWX_inverse_c_matrices[i];
      }
      delete[] W_matrices;
      delete[] cTXTWX_inverse_c_matrices;
  }
  else{ // If we use pre_assigned theta_1 value
    vector<double> v(dimensions[0] * dimensions[1] * dimensions[2]);
    fill(v.begin(), v.end(), theta_1);
    for(int i=0; i<num_of_C; i++){
      theta_1_C.push_back(v);
    }
  }


  /* two boolean arrays to indicate if this scan has cross upper or lower bounds for each C respectively*/
  bool cross_upper[num_of_C][dimensions[0] * dimensions[1] * dimensions[2]];
  bool cross_lower[num_of_C][dimensions[0] * dimensions[1] * dimensions[2]];
  /* set to false as default */
  for(int i = 0; i < num_of_C; i++){
    fill_n(cross_upper[i], dimensions[0] * dimensions[1] * dimensions[2], false);
    fill_n(cross_lower[i], dimensions[0] * dimensions[1] * dimensions[2], false);
  }


  int **scan_number_for_result_lower = new int *[num_of_C]; // stores the scan number this voxel first cross lower bound
  int **scan_number_for_result_upper = new int *[num_of_C]; // stores the scan number this voxel first cross upper bound
  /* initialize */
  for(int i=0; i<num_of_C; i++){
    scan_number_for_result_lower[i] = new int[dimensions[0] * dimensions[1] * dimensions[2]];
    fill_n(scan_number_for_result_lower[i], dimensions[0] * dimensions[1] * dimensions[2], 0);
    scan_number_for_result_upper[i] = new int[dimensions[0] * dimensions[1] * dimensions[2]];
    fill_n(scan_number_for_result_upper[i], dimensions[0] * dimensions[1] * dimensions[2], 0);
  }
  
  /* initialize opadd reducer counters */
  Int_Reducer cross_upper_bound_voxel_counter[num_of_C], cross_lower_bound_voxel_counter[num_of_C];

  bool crossed_easy = false; // whether crossed easy level bound
  bool swapped_matrix = false; // whether switched matrix yet

  /* Declare and intialize design matrix */
  X = new double[row * col];
  init_mat(X, row, col, file_path_x);

  double*** SPRT = new double**[row];
  double*** Z_score = new double**[row];
  for(int i = 0; i < row; i++){
    SPRT[i] = new double*[num_of_C];
    Z_score[i] = new double*[num_of_C];
    for(int j = 0; j < num_of_C; j++){
      SPRT[i][j] = new double[dimensions[0] * dimensions[1] * dimensions[2]]();
      Z_score[i][j] = new double[dimensions[0] * dimensions[1] * dimensions[2]]();
    }
  }

  double*** BETA_HAT = new double** [row];
  for(int i = 0; i < row; i++){
    BETA_HAT[i] = new double* [dimensions[0] * dimensions[1] * dimensions[2]];
    for(int j = 0; j < dimensions[0] * dimensions[1] * dimensions[2]; j++){
      BETA_HAT[i][j] = new double[col];
    }
  }


  /***************************************************************************
   *****                     Start of SPRT Analysis                      *****
   **************************************************************************/
  for(int scan_number = K_blk+1; scan_number <= row; scan_number++){
    /* initialized oppadd reducer counters to 0 */
    for(int i=0; i<num_of_C; i++){
      cross_upper_bound_voxel_counter[i].set_value(0);
      cross_lower_bound_voxel_counter[i].set_value(0);
    }
    
    /* Check existence of file */
    cout << "Waiting for bold" + to_string(scan_number-1)+".txt..." << endl;
    if(scan_number != row){
      while(!check_file_existence(file_path_y+"bold"+to_string(scan_number)+".txt")){
        // busy wait here
      }
    }
    else{
      while(!check_file_existence(file_path_y+"bold"+to_string(scan_number-1)+".txt")){
        /* busy wait here */
      }
      usleep(100); // wait 1000 milli seconds
    }
    cout << "File arrived. start reading in..." << endl;

    /* 2-D array response holds the response values for all data points for all time slots */
    read_scan(response, file_path_y, dimensions, scan_number-1);
    cout << "Read complete. " << scan_number << " scans have arrived. Starting SPRT analysis for them..." << endl;

    /* start counting time */
    chrono::duration<double> t; // for counting time difference
    chrono::high_resolution_clock::time_point t1 = chrono::high_resolution_clock::now();
    chrono::high_resolution_clock::time_point t2;

    compute_XTX_inverse(XTX_inverse, X, scan_number, col); // compute XTX_inverse for only once for each loop, here we truncate X matrix to scan_numer rows

    /* for each point in X-Y-Z space, compute the response array Y */
    cilk_for (int x = 0; x < dimensions[0]; x++) {
      for (int y = 0; y < dimensions[1]; y++) {
        for (int z = 0; z < dimensions[2]; z++) {
          //only prepare data and run SPRT when voxel is within ROI region *****
          if(subregion[x * dimensions[1] * dimensions[2] + y * dimensions[2] + z] != 0){
            int pos = x * dimensions[1] * dimensions[2] + y * dimensions[2] + z;
            double *Y = new double[scan_number * second_col]; // Initialize the response matrix

            // response array Y
            for (int i = 0; i < scan_number; i++) Y[i] = response[i][pos];
            
            compute_beta_hat(X, Y, beta_hat[pos], XTX_inverse, scan_number, col, second_col); 

            for(int i = 0; i < col; i++) BETA_HAT[scan_number-1][pos][i] = beta_hat[pos][i];

            sigma_hat_square_vector[pos] = estimate_sigma_hat_square(X, Y, beta_hat[pos], scan_number, col, second_col);
            if(is_within_ROI[pos] && subregion[pos] != 0)
              rho_map[pos] = compute_rho(Y, beta_hat[pos], X, sigma_hat_square_vector[pos], scan_number, col, second_col, K_blk);
            delete[] Y;
          }
        }
      }
    }


    cout << "Estimating new W-related matrices..." << endl;

    W_matrices = new double* [subregion_number+1];
    cTXTWX_inverse_c_matrices = new double*[subregion_number+1];
    for(int i = 0; i < subregion_number+1; i++){
      W_matrices[i] = new double[scan_number*scan_number];
      cTXTWX_inverse_c_matrices[i] =  new double[num_of_C];
    }

    generate_W_related_matrices(W_matrices, cTXTWX_inverse_c_matrices, X, dimensions, rho_map,
                                response, is_within_ROI, scan_number, col, second_col, subregion_number, 
                                subregion_count, subregion, C, num_of_C);

    cilk_for (int x = 0; x < dimensions[0]; x++) {
      for (int y = 0; y < dimensions[1]; y++) {
        for (int z = 0; z < dimensions[2]; z++) {
          //only prepare data and run SPRT when voxel is within ROI region *****
          if(subregion[x * dimensions[1] * dimensions[2] + y * dimensions[2] + z] != 0){
            int pos = x * dimensions[1] * dimensions[2] + y * dimensions[2] + z;
          
            double sprt[num_of_C];
            for(int i=0; i<num_of_C; i++){
              double* cT_beta_hat = new double[second_col];

              /* 
               * compute c'beta_hat 
               * used to estimate Z score
               */
  
              cblas_dgemm(
                    CblasRowMajor,
                    CblasTrans,
                    CblasNoTrans,
                    second_col,
                    second_col,
                    col,
                    1.0,
                    C[i],
                    second_col,
                    beta_hat[pos],
                    second_col,
                    0.0,
                    cT_beta_hat,
                    second_col
                    );
              // var( c'beta_hat ) = sigma_hat^2 * c' * (X'W_hat X)^(-1) * c
              double var = sigma_hat_square_vector[pos] * cTXTWX_inverse_c_matrices[subregion[pos]][i];
              Z_score[scan_number-1][i][pos] = cT_beta_hat[0] / sqrt(var);
              /* SPRT computation */
              sprt[i] = compute_SPRT(beta_hat[pos], col, C[i], theta_0, theta_1_C[i][pos], var);
              SPRT[scan_number-1][i][pos] = sprt[i]; // For testing purpose
              
              /* statistical processing */
              if (sprt[i] > max(boundary[0], boundary[1])){
                (*cross_upper_bound_voxel_counter[i])++;
                if(!cross_upper[i][pos]){
                  scan_number_for_result_upper[i][pos] = scan_number;
                  cross_upper[i][pos]= true;
                }
              }
              if (sprt[i] < min(boundary[0], boundary[1])){
                (*cross_lower_bound_voxel_counter[i])++;
                if(!cross_lower[i][pos]){
                  scan_number_for_result_lower[i][pos] = scan_number;
                  cross_lower[i][pos] = true;
                }
              }
              delete[] cT_beta_hat;
            }
          }
        }
      }
    }

    /* output to stdout */
    *cout_r << endl << endl << "---------------------- Round " << scan_number << ": -----------------------" << endl;
    *cout_r << "   " << "Lower boundary is " << min(boundary[0], boundary[1]) << ". Upper boundary is " << max(boundary[0], boundary[1]) << endl;
    for(int i=0; i<num_of_C; i++){
      *cout_r << "            " << (double) cross_upper_bound_voxel_counter[i].get_value() << " (" << 100 * (double) cross_upper_bound_voxel_counter[i].get_value() / (double) (ROI_voxel_number) << "%) crossed upperbound for C" << i+1 << endl;
      *cout_r << "            " << (double) cross_lower_bound_voxel_counter[i].get_value() << " (" << 100 * (double) cross_lower_bound_voxel_counter[i].get_value() / (double) (ROI_voxel_number) << "%) crossed lowerbound for C" << i+1 << endl;
    }

    /* Stop ticking and ouput timer result */
    t2 = chrono::high_resolution_clock::now();
    t = t2 - t1;
    cout << "            Processed in " << t.count() << " seconds" << endl;
    cout << "---------------------------------------------------------" << endl << endl;

    myfile1 << scan_number << ",";
    for(int i=0; i<num_of_C; i++){
      myfile1 << (double) cross_upper_bound_voxel_counter[i].get_value() << "," << (double) cross_lower_bound_voxel_counter[i].get_value() << ",";
    }
    
    myfile1 << t.count() << endl;

    /* 
     * Output into file for trigger pulse and then do cleanup
     * Format: scan_number c_matrix_number activation_code
     * scan_number: number of scans
     * c_matrix_number: either C1 or C2 or C3
     * activation code: 0 - cross lower bound
     *                  1 - cross upper bound
     *                  2 - cross both bounds in a single scan
     *                 -1 - within bounds
     */
    if(scan_number > K_blk){
      myfile2 << scan_number << ",";
      for(int i = 0; i < num_of_C; i++){
        if(((double) cross_upper_bound_voxel_counter[i].get_value() 
        + (double) cross_lower_bound_voxel_counter[i].get_value()) 
        / (double) (ROI_voxel_number) > crossed_easy_percent)
          myfile2 <<  "Stop,";
        else myfile2 << "Continue,";
    
        if(((double) cross_upper_bound_voxel_counter[1].get_value() 
        + (double) cross_lower_bound_voxel_counter[1].get_value()) 
        / (double) (ROI_voxel_number) > crossed_hard_percent) 
          myfile2 <<  "Stop,";
        else myfile2 << "Continue,";
      }
      myfile2 << endl;

      /* If we have not swiched design matrix, wait for the file
       * indicating which scan it switch the difficulty level,
       * then swap the design matrix accordingly
       * Swapping matrix runs only once
       */
       
      if(crossed_easy && !swapped_matrix){
        cout << "Detects crossed bound(s)." << endl;
        usleep(100); // sleep 100ms for files to transfer back from lumia box controller.
        if(check_file_existence("./Latest_data/found_activation_stopeasy.txt")){
          ifstream in("./Latest_data/found_activation_stopeasy.txt");
          int n;
          in >> n;
          in.close();
          assemble_new_degign_matrix("./Latest_data/design_easy.txt", "./Latest_data/design_hard.txt", "./Latest_data/design_new.txt", row, col, n);
          init_mat(X, row, col, "./Latest_data/design_new.txt");
          swapped_matrix = true;
          cout << "Swap design matrix at " << n << " scans" << endl;
          myfile2 << "Swap design matrix at " << n << " scans" << endl;
        }
      }
    }
    

    // generate test files for the scan using estimate_theta1 funtion
    if(find(scans_to_generate_test_output.begin(), scans_to_generate_test_output.end(), scan_number) != scans_to_generate_test_output.end()){ // scan_number exists in scans_to_generate_test_output
      cout << "Generating test file ...";
      
      for(int i=0; i<num_of_C; i++){
        for(int j = 0; j < subregion_number+1; j++) cTXTWX_inverse_c_vector[j] = cTXTWX_inverse_c_matrices[j][i];

        estimate_theta1(beta_hat, response, dimensions, C[i], scan_number, col, second_col, Z, W_matrices, 
                        cTXTWX_inverse_c_vector, is_within_ROI, subregion, sigma_hat_square_vector, true);
      
      }
      cout << " Done!" << endl;
    }

    /*
     * For test purpose
     * output sigma_hat (not square) at each scan
     */
    bool output_test_for_each_scan = true;
    if(output_test_for_each_scan){
      for(int i=0; i<num_of_C; i++){
        for(int j = 0; j < subregion_number+1; j++) cTXTWX_inverse_c_vector[j] = cTXTWX_inverse_c_matrices[j][i];

        estimate_theta1(beta_hat, response, dimensions, C[i], scan_number, col, second_col, Z, W_matrices, 
                        cTXTWX_inverse_c_vector, is_within_ROI, subregion, sigma_hat_square_vector, true);
      
      }
    }

    /* ********************************************************************************************************** */
    /* output beta_hat of each scan */
    for(int i=0; i<col; i++){
      myfile3.open("./test_files/" + to_string(scan_number) + "/beta_hat at regressor " + to_string(i) + ".txt");
      for(int x = 0; x < dimensions[0]; x++){
        myfile3 << "Slice " << x << ": " << endl;
        for(int y = 0; y < dimensions[1]; y++){
          for(int z = 0; z < dimensions[2]; z++){
            myfile3 << beta_hat[x * dimensions[1] * dimensions[2] + y * dimensions[2] + z][i] << " ";
          }
          myfile3 << endl;
        }
        myfile3 << endl << endl << endl;
      }
      myfile3.close();
    }

    clear_2d(beta_hat, dimensions[0] * dimensions[1] * dimensions[2], col * second_col);

    for(int i = 0; i < subregion_number+1; i++){
      delete[] W_matrices[i];
      delete[] cTXTWX_inverse_c_matrices[i];
    }
    delete[] W_matrices;
    delete[] cTXTWX_inverse_c_matrices;
    
    /* ********************************************************************************************************** */

  } // end of SPRT computation

  /* Close SPRT process file */
  myfile1.close();
  myfile2.close();

  /* 
   * Output Voxel Level Info: Activation 
   * of first scan on over upper/lower bound
   */
  for(int i=0; i<num_of_C; i++){
    myfile1.open("Voxel Level Info for contrast " + to_string(i+1) + ".csv");
    myfile1 << "Voxel Number,First Scan Cross Lower,First Scan Cross Upper" << endl;
    for (int x = 0; x < dimensions[0]; x++) {
      for (int y = 0; y < dimensions[1]; y++) {
        for (int z = 0; z < dimensions[2]; z++) {
          if(scan_number_for_result_upper[i][x * dimensions[1] * dimensions[2] + y * dimensions[2] + z] != 0 
          || scan_number_for_result_lower[i][x * dimensions[1] * dimensions[2] + y * dimensions[2] + z] != 0)
            myfile1 <<"[" << setw(3) 
                    << x
                    << setw(3) 
                    << y 
                    << setw(3) 
                    << z 
                    << "],"  
                    << scan_number_for_result_lower[i][x * dimensions[1] * dimensions[2] + y * dimensions[2] + z]
                    << ","
                    << scan_number_for_result_upper[i][x * dimensions[1] * dimensions[2] + y * dimensions[2] + z] 
                    << endl;
        }
      }
    }
    myfile1.close();
  }

  /*
   * Output SPRT time series data
   */
  for(int i = 0; i < num_of_C; i++){
    myfile1.open("SPRT time series for contrast " + to_string(i+1) + ".csv");
    for (int x = 0; x < dimensions[0]; x++) {
      for (int y = 0; y < dimensions[1]; y++) {
        for (int z = 0; z < dimensions[2]; z++) {
          myfile1 <<"[" << setw(3) 
                    << x
                    << setw(3) 
                    << y 
                    << setw(3) 
                    << z 
                    << "],";
          for(int n = 0; n < row; n++){
            myfile1 << SPRT[n][i][x * dimensions[1] * dimensions[2] + y * dimensions[2] + z] << ",";
          }
          myfile1 << endl;
        }
      }
    }
    myfile1.close();
  }

  cout << "Do you want to output time series data for specific voxels?"
       << "\nIf yes, please put voxel coordinates into a file named"
       << "\n\"voxels_list.txt\" separated by line, the format for each voxel is"
       << "\nX Y Z" << endl;
  char output_specific_voxles;
  cin >> output_specific_voxles;
  if(toupper(output_specific_voxles) == 'Y'){
    cout << "Which scan number would you use to show the impact of rho to Z score? ";
    int scan_number = 150;
    cin >> scan_number;
    int coor;
    ifstream myfile("voxels_list.txt");
    vector<int> voxels_list;
    while(myfile >> coor){
      voxels_list.push_back(coor);
    }
    if(voxels_list.size() % 3 == 0){
      for(int i=0; i<num_of_C; i++){
        int pos = 0;
        myfile1.open("Specific_voxel_Info for contrast " + to_string(i+1) + ".csv");
        myfile1 << "Set Z score:," << Z << endl;
        myfile1 << "SPRT set upper bound:," <<  max(boundary[0], boundary[1]) << endl;
        myfile1 << "SPRT set lower bound:," << min(boundary[0], boundary[1]) << endl;
        myfile1 << "SPRT Value Time Series Data" << endl;
        myfile1 << "X,Y,Z,";
        for(int scan = K_blk+1; scan <= row; scan++) myfile1 << "Scan" << scan << ",";
        myfile1 << endl;
        for(int in = 0; in < voxels_list.size()/3; in++){
          pos = voxels_list[in*3] * dimensions[1] * dimensions[2] + voxels_list[in*3+1] * dimensions[2] + voxels_list[in*3+2];
          myfile1 << voxels_list[in*3] << "," << voxels_list[in*3+1] << "," << voxels_list[in*3+2] << ",";
          for(int n = K_blk; n < row; n++){
            myfile1 << SPRT[n][i][pos] << ",";
          }
          myfile1 << endl;
        }
        myfile1 << endl;

        myfile1 << "Z Score Time Series Data" << endl;
        myfile1 << "X,Y,Z,";
        for(int scan = K_blk+1; scan <= row; scan++) myfile1 << "Scan" << scan << ",";
        myfile1 << endl;
        for(int in = 0; in < voxels_list.size()/3; in++){
          pos = voxels_list[in*3] * dimensions[1] * dimensions[2] + voxels_list[in*3+1] * dimensions[2] + voxels_list[in*3+2];
          myfile1 << voxels_list[in*3] << "," << voxels_list[in*3+1] << "," << voxels_list[in*3+2] << ",";
          for(int n = K_blk; n < row; n++){
            myfile1 << Z_score[n][i][pos] << ",";
          }
          myfile1 << endl;
        }
        myfile1 << endl << endl << endl << endl;

        myfile1 << "Set Z score:," << Z << endl;
        myfile1 << "SPRT set upper bound:," <<  max(boundary[0], boundary[1]) << endl;
        myfile1 << "SPRT set lower bound:," << min(boundary[0], boundary[1]) << endl;
        myfile1 << "SPRT Value Time Series Data" << endl;
        myfile1 << "Note: * after value means over set SPRT upper bound, * before value means below set SPRT lower bound." << endl;
        myfile1 << "If voxel X corrdinates are padded with *, it means this voxel is not within subregion";
        myfile1 << "X,Y,Z,";
        for(int scan = K_blk+1; scan <= row; scan++) myfile1 << "Scan" << scan << ",";
        myfile1 << endl;
        for(int in = 0; in < voxels_list.size()/3; in++){
          pos = voxels_list[in*3] * dimensions[1] * dimensions[2] + voxels_list[in*3+1] * dimensions[2] + voxels_list[in*3+2];
          if(subregion[pos] == 0) myfile1 << voxels_list[in*3] << " *," << voxels_list[in*3+1] << "," << voxels_list[in*3+2] << ",";
          else myfile1 << voxels_list[in*3] << "," << voxels_list[in*3+1] << "," << voxels_list[in*3+2] << ",";
          for(int n = K_blk; n < row; n++){
            if(SPRT[n][i][pos] > max(boundary[0], boundary[1])) myfile1 << SPRT[n][i][pos] << " *" << ",";
            else if(SPRT[n][i][pos] < min(boundary[0], boundary[1])) myfile1 << "* " << SPRT[n][i][pos] << ",";
            else myfile1 << SPRT[n][i][pos] << ",";
          }
          myfile1 << endl;
        }
        myfile1 << endl;

        myfile1 << "Z Score Time Series Data" << endl;
        myfile1 << "Note: * after value means over set Z score." << endl;
        myfile1 << "X,Y,Z,";
        for(int scan = K_blk+1; scan <= row; scan++) myfile1 << "Scan" << scan << ",";
        myfile1 << endl;
        for(int in = 0; in < voxels_list.size()/3; in++){
          pos = voxels_list[in*3] * dimensions[1] * dimensions[2] + voxels_list[in*3+1] * dimensions[2] + voxels_list[in*3+2];
          if(subregion[pos] == 0) myfile1 << voxels_list[in*3] << " *," << voxels_list[in*3+1] << "," << voxels_list[in*3+2] << ",";
          else myfile1 << voxels_list[in*3] << "," << voxels_list[in*3+1] << "," << voxels_list[in*3+2] << ",";
          for(int n = K_blk; n < row; n++){
            if(Z_score[n][i][pos] > Z) myfile1 << Z_score[n][i][pos] << " *" << ",";
            else myfile1 << Z_score[n][i][pos] << ",";
          }
          myfile1 << endl;
        }
        myfile1 << endl << endl;

        myfile1 << "Rho value impact to Z score" << endl;
        myfile << "Scan number " << scan_number << " is used" << endl;
        myfile1 << "Note: * after value means over set SPRT upper bound, * before value means below set SPRT lower bound." << endl;
        myfile1 << "Rho,,,";
        for(double step = 0.0; step/1000 != 1.0; step++){
          myfile1 << step/1000 << ",";
        }
        myfile1 << endl;
        for(int in = 0; in < voxels_list.size()/3; in++){
          pos = voxels_list[in*3] * dimensions[1] * dimensions[2] + voxels_list[in*3+1] * dimensions[2] + voxels_list[in*3+2];
          if(subregion[pos] == 0) myfile1 << voxels_list[in*3] << " *," << voxels_list[in*3+1] << "," << voxels_list[in*3+2] << ",";
          else myfile1 << voxels_list[in*3] << "," << voxels_list[in*3+1] << "," << voxels_list[in*3+2] << ",";
          double *Y = new double[scan_number * second_col]; // Initialize the response matrix
          for (int l = 0; l < scan_number; l++) Y[l] = response[i][pos];
          double sigma_hat = estimate_sigma_hat_square(X, Y, BETA_HAT[scan_number-1][pos], scan_number, col, second_col);
          for(double step = 0.0; step/1000 != 1.0; step++){
            //cout << step << endl;
            double temp = testing_Z_score_impact_by_rho(pos, sigma_hat, C[i], BETA_HAT, X, scan_number, col, second_col, step/1000);
            myfile1 << temp << ",";
          }
          myfile1 << endl;
          delete[] Y;
        }
        myfile1.close();
      }
    }
    else cout << "Error: Coordinates Info Incorrect. Check Your File!";
  }

  /*
   * Output voxel wise activation status
   */
  for(int i=0; i<num_of_C; i++){
    myfile1.open("Voxel Activation Status for contrast " + to_string(i+1) + ".csv");
    for (int x = 0; x < dimensions[0]; x++) {
      for (int y = 0; y < dimensions[1]; y++) {
        for (int z = 0; z < dimensions[2]; z++) {
          if(scan_number_for_result_upper[i][x * dimensions[1] * dimensions[2] + y * dimensions[2] + z] == 0 
          && scan_number_for_result_lower[i][x * dimensions[1] * dimensions[2] + y * dimensions[2] + z] == 0)
            myfile1 << "0,";
          else if(scan_number_for_result_upper[i][x * dimensions[1] * dimensions[2] + y * dimensions[2] + z] == 0 
               && scan_number_for_result_lower[i][x * dimensions[1] * dimensions[2] + y * dimensions[2] + z] != 0)
            myfile1 << "1,";
          else if(scan_number_for_result_upper[i][x * dimensions[1] * dimensions[2] + y * dimensions[2] + z] != 0 
               && scan_number_for_result_lower[i][x * dimensions[1] * dimensions[2] + y * dimensions[2] + z] == 0)
            myfile1 << "2,";
          else
            myfile1 << "3,";
        }
        myfile1 << endl;
      }
      myfile1 << endl;
    }
    myfile1.close();
  }

  /* delete dynamic allocated memory */

  delete[] X;
  delete[] XTX_inverse;

  delete[] cTXTWX_inverse_c_vector;

  for(int i = 0; i < dimensions[0] * dimensions[1] * dimensions[2]; i++){
    delete[] beta_hat[i];
  }
  delete[] beta_hat;

  for(int i=0; i<num_of_C; i++){
    delete[] C[i];
    delete[] scan_number_for_result_lower[i];
    delete[] scan_number_for_result_upper[i];
  }
  delete[] C;
  delete[] scan_number_for_result_lower;
  delete[] scan_number_for_result_upper;

  // delete 3 dimension array
  for(int i = 0; i < row; i++){
    for(int j = 0; j < dimensions[0] * dimensions[1] * dimensions[2]; j++)
      delete[] BETA_HAT[i][j];

    for(int j = 0; j < num_of_C; j++){
      delete[] SPRT[i][j];
      delete[] Z_score[i][j];
    }
    delete[] SPRT[i];
    delete[] Z_score[i];
    delete[] BETA_HAT[i];
  }
  delete[] SPRT;
  delete[] Z_score;
  delete[] BETA_HAT;


  return 0;
}
