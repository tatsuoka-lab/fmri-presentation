/*
 * This code is customized by Weicong and Curtis to show a module to
 * handle the weight lifting part and computes the
 * formula 3 in paper "Dynamic adjustment of stimuli in real time functional
 * magnetic resonance imaging"
 */

#include "numerical.h"
using namespace std;

int SCAN_SIZE;
int DIMENSION_SIZE;

/*
 * Clear 2D array
 */
void clear_2d(double **mat, int row, int col) {
    for(int i=0; i<row; i++) fill_n(mat[i], col, 0);
}

/**
 * Param: matrix
 * Param: row dimension
 * Param: col dimension
 * Functionality: takes in an matrix and output each slot.
 */
void show_matrix(double *mat, int row, int col) {
    //cout.precision(17);
    for (int i = 0; i < row; i++) {
        for (int j = 0; j < col; j++) {
            cout << fixed << mat[i * col + j] << " ";
        }
        cout << endl;
    }
    cout << endl;
}

/**
  * Param: response vector which has the dimension SCAN_SIZE * [VOXEL_SIZE_1 * VOXEL_SIZE_2 * VOXEL_SIZE_2]
  * Param: file_num
  * Param: slice number
  * Param: Row number
  * Param: Col number
  * Functionality: takes in a nested vector and output each slot for select files;
  */
void show_response(vector<vector<double>> response, vector<int> dimensions, int file_num, int Z, int X, int Y){
    cout << "File number: " 
         << file_num
         << endl 
         << "Slice number: " 
         << Z << endl 
         << "Voxel location: [" 
         << X 
         << ", " 
         << Y << "]." 
         << "Value stored is :" 
         << response[file_num-1][Z * (dimensions[1] + 1) * (dimensions[2] + 1) + X * (dimensions[2] + 1) + Y]
         <<endl;
}

/*
 * Param: matrix
 * Param: row dimension
 * Param: col dimension
 * Functionality: takes in an matrix and initiate each slot with the
 * values obtained from file, as specified by file path.
 */
void init_mat(double *mat, int row, int col, string filepath) {
    ifstream data (filepath.c_str(), ios::in);
    
    if (!data) {
        cout << "File " << filepath << " could not be opened." << endl;
        exit(1);
    }
    
    for(int i = 0; i < row; i++) {
        for(int j = 0; j < col; j++) {
            data >> mat[i*col+j];
        }
    }
    
    data.close();
}

/*
 * Computes the inverse of a symmetric (Hermitian) positive-definite 
 * matrix using the Cholesky factorization
 */


void calc_inverse(double *matrix, int n){
    int info;

    /* before computing the inversion of the matrix, we need first to factor the matrix */
    dpotrf("U", &n, matrix, &n, &info);

    if(info != 0){
        cerr << "Cholesky factorization failed. Error code " << info << ". Exiting..." << endl;
        exit(0);
    }
    
    dpotri("U", &n, matrix, &n, &info);

    if(info != 0){
        cerr << "Computing inverse of matrix failed. Error code " << info <<". Exiting..." << endl;
        exit(0);
    }

    for(int i=0; i<n; i++){
        for(int j=0; j<n; j++){
            matrix[i*n+j] = matrix[j*n+i];
        }
    }
}


/* 
 * LU factorize and then compute matrix inverse using dgetri
 * Slower than Cholesky and possess some issue
 */
void calc_inverse_dgetri(double *matrix, int n){

    int* ipiv = new int[n];
    int info;
    
    // before computing the inversion of the matrix, we need first to factor the
    // matrix
    dgetrf(&n, &n, matrix, &n, ipiv, &info);

    if(info != 0){
        cerr << "LU factorization failed. Exiting..." << endl;
        exit(0);
    }
    
    double _tmp;

    /*
     * If lwork = -1, then a workspace query is assumed; 
     * the routine only calculates the optimal size of 
     * the work array, returns this value as the first entry 
     * of the work array, and no error message related to lwork 
     * is issued by xerbla.
     */
    int _lwork = -1;
    dgetri(&n, matrix, &n, ipiv, &_tmp, &_lwork, &info);
    _lwork = (int)_tmp;

    if(info != 0){
        cerr << "Calculating optimal size of the work array fails! Exiting..." << endl;
        exit(0);
    }
    
    double *work = new double[_lwork];
    dgetri(&n, matrix, &n, ipiv, work, &_lwork, &info);

    if(info != 0){
        cerr << "Calculating inverse of matrix fails! Exiting..." << endl;
        exit(0);
    }
}

/*
 * Output: SPRT result
 * Functioality: this function is to handle the weight lifting part and computes
 * the formula 3 in paper "Dynamic adjustment of stimuli in real time functional
 * magnetic resonance imaging"
 * Formula: {(c*beta_hat-theta_0)'* Var(c*beta_hat)^-1 * (c*beta_hat-theta_0) 
 *          - (c*beta_hat-theta_1)'* Var(c*beta_hat)^-1 * (c*beta_hat-theta_1)} / 2
 */
double compute_SPRT(
                double *beta_hat, int col, double *c, double thetaZero, double thetaOne, double var_cBeta_hat
                ) {
    double result = 0;
    double CB = 0; // store c * beta_hat
    for (int i = 0; i < col ; i++) {
        CB += beta_hat[i] * c[i];
    }
    return ( pow(CB - thetaZero, 2) - pow(CB - thetaOne, 2) ) / (2 * var_cBeta_hat);
}

/**
 * Param: a string in the header of each response file, e.g. in bold113.txt
 *   it is (36, 128, 128)
 *   or a string in the header of design matrix, e.g. (320, 10)
 * Output: the X-Y-Z dimension values of response file, e.g. 36, 128, 128
 *   or the row-column values of the design matrix
 * Functionality: this method takes in a string composed of dimension parameters
 *   and output three integers indicating the value of each dimension
 */
vector<int> dimension_parser(const string &s) {
    int num = 0;
    vector<int> dimensions;
    bool lastIsDigit = false;
    for (int i = 0; i < s.size(); i++) {
        if (isdigit(s[i])) {
            num *= 10;
            num += s[i] - '0';
            lastIsDigit = true;
        } else {
            if (lastIsDigit) {
                dimensions.push_back(num);
                num = 0;
                lastIsDigit = false;
            }
        }
    }
    //cout << dimensions[0] << " " << dimensions[1] << endl;
    return dimensions;
}


/**
 * This method wrote by Yi is actually incorrect. For example, say a string "1, 2, 3", 
 * it will actually parse it as numbers 1, 12, 123 instead of 1, 2, 3.
 */

/*
 * Param: a string of line containing numbers segmented by ','
 * Output: a vector of integers parsed by ','
 * Functionality: this method takes in a string, say "1,2,3" and parse it as
 * numbers separated by ',' and then return the numbers to the vector */
 
//vector<double> parse_voxel_value(string line) {
//    vector<double> ret;
//    double temp = 0;
//    for (int i = 0; i < line.size(); i++) {
//        if (isdigit(line[i])) {
//            temp *= 10;
//            temp += line[i] - '0';
//        } else {
//            ret.push_back(temp);
//        }
//    }
//    ret.push_back(temp);
//    return ret;
//}


/**
 * Param: a string of line containing numbers segmented by ','
 * Output: a vector of integers parsed by ','
 * Functionality: this method takes in a string, say "1,2,3" and parse it as
 *   numbers separated by ',' and then return the numbers to the vector
 */
vector<double> parse_voxel_value(string line) {
    istringstream ss (line);
    vector <double> ret;
    while(ss){
        string s; 
        if(!getline(ss, s, ',')) break;
        ret.push_back(stod(s));
    }
    return ret;
}

/**
 * Param: a nested vector of double type which contains the response matrix
 * Param: a string identifying the bold text file location
 * Param: a vector of int which contains the dimension information
 * Param: a counter of int to specify the latest scan number when doing real time analysis
 * Output: none
 * Functionality: For time-series issue, the single response vector is nested into 2-dimensional array.
 *    Each file is a row response and SCAN_SIZE files means SCAN_SIZE rows of response.
 */
void read_scan(vector<vector<double> > &response, string file_path, vector<int> &dimensions, int scan_number) {
    file_path+="bold"+to_string(scan_number)+".txt";
    ifstream myfile (file_path);
    string line = "";
    
    //cout << "Reading file " << file_path << endl;

    if (myfile.is_open()) {
        getline(myfile, line);
        
        /* Z - X - Y */
        dimensions = dimension_parser(line);
        
        /* get the maximum element as the base value, here (VOXEL_SIZE_2 + 1) */
        vector<double> ret (dimensions[0] * dimensions[1] * dimensions[2], 0);
        int X = 0, Y = 0, Z = -1;
        while(!myfile.eof()) {
            getline(myfile, line);
            
            /* meanning if we can find a match of word "slice" in the string */
            if (strstr(line.c_str(), "Slice") != NULL) {
                /* this is a line like "#new slice" */
                Z++;
                /* reset x axis to zero */
                X = 0;
            } else {
                /* this is a line containing numbers */
                /* reset Y to 0 */
                Y = 0;
                vector<double> line_tokens = parse_voxel_value(line);
                
                for (; Y < line_tokens.size(); Y++) {
                    // cout << "Z is" << Z << " X is " << X << " Y is " << Y <<  endl;
                    ret[Z * dimensions[1] * dimensions[2] + X * dimensions[2] + Y] = line_tokens[Y];
                }
                
                X++;
            }
        }
        /* a 2-D row here means a time-stamp */
        response.push_back(ret);
    } else {
        cout << "Can't find file " << file_path << endl;
        throw "File open error at reading matrix.";
    }
    
    myfile.close();
}

/**
 * Param: string of file path
 * Param: vector of integer which contains dimensions
 * Output: a 2-D array encoding response values
 * Functionality: this method simply runs on top of method read_scan and
 *   enumerate all possible file names and read them, store the values in the
 *   2-D response vector.
 *   Each file corresponds to the response for a certain time slot and makes up
 *   a row in 2-D array.
 */
vector<vector<double> > read_all_scans(string file_path_y, vector<int> &dimensions) {
    vector<vector<double> > response;
    for (int i = 1; i <= 100; i++) {
        string temp = file_path_y + "bold";
        
        //if (i < 10) {
        //    temp += "000";
        //    temp.append(1, i + '0');
        //} else if (i >= 10 && i <= 99) {
        //    temp += "00";
        //    string temp_second = "";
        //    int k = i;
        //    while (k > 0) {
        //        temp_second.append(1, k % 10 + '0');
        //        k /= 10;
        //    }
        //    reverse(temp_second.begin(), temp_second.end());
        //    temp += temp_second;
        //} else { // i >= 100
        //    //temp += "0";
        //    string temp_second = "";
        //    int k = i;
        //    while (k > 0) {
        //        temp_second.append(1, k % 10 + '0');
        //        k /= 10;
        //    }
        //    reverse(temp_second.begin(), temp_second.end());
        //    temp += temp_second;
        //}

        temp += to_string(i);
        temp += ".txt";
        // once the file names is generated, read contents from them
        read_scan(response, temp, dimensions, 0);
    }
    return response;
}

/**
 * Param: double -- alpha, double -- beta
 * Output: the stopping rule's boundaries [A, B]
 * Functionality: this method takes in two parameters, alpha and bate, and
 *   computes the stopping rule's boundary values A and B
 */
vector<double> stop_boundary(double alpha, double beta) {
    double A = log((1 - beta) / alpha), B = log(beta / (1 - alpha));
    vector<double> boundary;
    boundary.push_back(A);
    boundary.push_back(B);
    return boundary;
}

/*
 * Functionality: check for file existence for real-time analysis
 */
bool check_file_existence (const string& name) {
  struct stat buffer;   
  return (stat (name.c_str(), &buffer) == 0); 
}

/**
 * Param: int -- scan_number, int -- n, int -- k, int -- Z
 * Functionality: Dynamically determine theta_1.
 * At the voxel level, after estimating sigma^2 for each voxel. After first scan_number blocks,
 * Let us estimate sigma^2 by matrix computations.
 * sigma_hat ^2 = sum{r_i^2} / (# of scans - # of parameters)
 * r_i = Y_i - X * beta_hat
 * The estimated covariance matrix for c'beta_hat ( Var(c'beta_hat) ) is sigma_hat^2 * c'(X'WX)^{-1}c.
 * Once we get estimate after K scans, let's compute  atarget theta_1 for each voxel in each contrast.
 * We do this as follows: theta_1 = Z * sqrt ( Var(c'beta_hat) ) 
 * We only set theta_1 once, for each voxel. Z is a universal value across the brain.
 */
vector<double> estimate_theta1(double** beta_hat, const vector<vector<double>>& response, const vector<int>& dimensions, double *c, int scan_number,
                              int col, int second_col, double Z, double** W_matrices, double* cTXTWX_inverse_c_vector, 
                              const vector<bool>& is_within_ROI, const vector<int>& subregion, 
                              vector<double> &sigma_hat_square_vector, bool write_out_test_file){
    vector<double> theta_1(dimensions[0] * dimensions[1] * dimensions[2], 0.0);

    /* for each point in X-Y-Z space, compute the response array Y */
    cilk_for (int x = 0; x < dimensions[0]; x++) {
      for (int y = 0; y < dimensions[1]; y++) {
        for (int z = 0; z < dimensions[2]; z++) {
          int pos = x * dimensions[1] * dimensions[2] + y * dimensions[2] + z;
          if(is_within_ROI[pos] && subregion[pos] != 0){
            theta_1[pos] = Z * sqrt(sigma_hat_square_vector[pos] * cTXTWX_inverse_c_vector[subregion[pos]]);
          }
        }
      }
    }

    /*
     * Generate test output file
     * Output variables: 
     * sigma_hat: formula is shown in above comments
     * sqrt(var(c'beta_hat)): sqrt(var (c'beta_hat)) = sqrt(sigma_hat^2 * c'(X'WX)^-1c)
     * Z score: Z = c'beta_hat / sqrt(var (c'Beta_hat))
     */
    if(write_out_test_file){
        vector<double> cT_beta_hat_vector(dimensions[0] * dimensions[1] * dimensions[2], 0.0);
        cilk_for (int x = 0; x < dimensions[0]; x++) {
          for (int y = 0; y < dimensions[1]; y++) {
            for (int z = 0; z < dimensions[2]; z++) {
              int pos = x * dimensions[1] * dimensions[2] + y * dimensions[2] + z;
              double *cT_beta_hat = new double[second_col];

              /* 
               * compute c'beta_hat 
               * used to estimate Z score
               */
              cblas_dgemm(
                    CblasRowMajor,
                    CblasTrans,
                    CblasNoTrans,
                    second_col,
                    second_col,
                    col,
                    1.0,
                    c,
                    second_col,
                    beta_hat[pos],
                    second_col,
                    0.0,
                    cT_beta_hat,
                    second_col
                    );

              cT_beta_hat_vector[pos] = cT_beta_hat[0];
              delete[] cT_beta_hat;
            }
          }
        }
        vector<double> var_cBeta_hat_vector(dimensions[0] * dimensions[1] * dimensions[2], 0.0);
        vector<double> Z_score_vector(dimensions[0] * dimensions[1] * dimensions[2], 0.0);

        
        for (int x = 0; x < dimensions[0]; x++) {
          for (int y = 0; y < dimensions[1]; y++) {
            for (int z = 0; z < dimensions[2]; z++) {
              int pos = x * dimensions[1] * dimensions[2] + y * dimensions[2] + z;
              if(is_within_ROI[pos] && subregion[pos] != 0){
                var_cBeta_hat_vector[pos] = sigma_hat_square_vector[pos] * cTXTWX_inverse_c_vector[subregion[pos]];
                Z_score_vector[pos] = cT_beta_hat_vector[pos] / sqrt(var_cBeta_hat_vector[pos]);
              }
            }
          }
        }
        
        write_out_voxel_level_info_to_file("./test_files/" 
                                          + to_string(scan_number) 
                                          +"/sigma_hat_square.txt", 
                                          sigma_hat_square_vector, 
                                          dimensions);

        write_out_voxel_level_info_to_file("./test_files/" 
                                          + to_string(scan_number) 
                                          +"/sqrt(var(c * beta_hat)) with C [" 
                                          + to_string(int(c[0])) 
                                          + ", " 
                                          + to_string(int(c[1])) 
                                          + ", " 
                                          + to_string(int(c[2])) 
                                          + "].txt", 
                                          var_cBeta_hat_vector, 
                                          dimensions);

        write_out_voxel_level_info_to_file("./test_files/" 
                                          + to_string(scan_number) 
                                          +"/Z_score with C [" 
                                          + to_string(int(c[0])) 
                                          + ", " + to_string(int(c[1])) 
                                          + ", " + to_string(int(c[2])) 
                                          + "].txt", 
                                          Z_score_vector, 
                                          dimensions);

        write_out_voxel_level_info_to_file("./test_files/" 
                                          + to_string(scan_number) 
                                          +"/theta_1 with C [" 
                                          + to_string(int(c[0])) 
                                          + ", " + to_string(int(c[1])) 
                                          + ", " + to_string(int(c[2])) 
                                          + "].txt", 
                                          theta_1, 
                                          dimensions);
    }

    return theta_1;
}

double testing_Z_score_impact_by_rho(int pos, double sigma_hat, double* c, double*** BETA_HAT, double* X, 
                                     int scan_number, int col, int second_col, double rho){

  // compute c'beta_hat
  double* cT_beta_hat = new double[second_col];

  cblas_dgemm(
              CblasRowMajor,
              CblasTrans,
              CblasNoTrans,
              second_col,
              second_col,
              col,
              1.0,
              c,
              second_col,
              BETA_HAT[scan_number-1][pos],
              second_col,
              0.0,
              cT_beta_hat,
              second_col
              );

  // generate W matrix using new rho
  double* W = new double[scan_number * scan_number];
  double* XTWX_inverse = new double[col * col];

  generate_W_matrix(W, rho, scan_number);
  compute_XTWX_inverse(W, X, XTWX_inverse, scan_number, col);
  double cTXTWX_inverse_c = compute_cTXTWX_inverse_c(XTWX_inverse, c, col, second_col);

  double res =  cT_beta_hat[0] / sqrt(sigma_hat * cTXTWX_inverse_c);
  delete[] cT_beta_hat;
  delete[] W;
  delete[] XTWX_inverse;

  return res;
}

/*
 * estimate sigma_hat^2
 * sigma_hat ^2 = sum{r_i^2} / (# of scans - # of parameters)
 * r_i = Y_i - X * beta_hat
 */
double estimate_sigma_hat_square(const double* X, const double* Y, const double* beta_hat, int scan_number, int col, int second_col){

    double * XT_beta_hat = new double[scan_number * second_col];

    cblas_dgemm(
           CblasRowMajor,
           CblasNoTrans,
           CblasNoTrans,
           scan_number,
           second_col,
           col,
           1.0,
           X,
           col,
           beta_hat,
           second_col,
           0.0,
           XT_beta_hat,
           second_col
           );

    double res = 0;
    for(int i = 0; i < scan_number; i++) 
        res += pow(Y[i] - XT_beta_hat[i], 2);
    res /= (scan_number-col);
    delete[] XT_beta_hat;

    return res;
}


/*
 * DEPRECATED 9/3/19
 * not using this formula anymore
 * compute sigma_hat^2
 * sigma_hat ^2 = SSE / (# of scans - # of parameters)
 * SSE = Y'Y - beta'X'Y. update: Mar 9 2019: SSE = Y'WY - Y'WX * beta_hat. update: Mar 18 2019: SSE = Y'Y - Y'X * beta_hat
 */
double compute_sigma_hat_square_old(const double *X, const double *Y, double* YTX, const double* beta_hat, double *YTX_beta_hat, 
                                int scan_number, int col, int second_col){

      // compute Y'Y
      double Y_total = 0;
      for(int i = 0; i < scan_number; i++){
        Y_total += pow(Y[i], 2);
      }

      // compute Y'X
      cblas_dgemm(
           CblasRowMajor,
           CblasTrans,
           CblasNoTrans,
           second_col,
           col,
           scan_number,
           1.0,
           Y,
           second_col,
           X,
           col,
           0.0,
           YTX,
           col
           );

      // compute Y'X * beta_hat
      cblas_dgemm(
                 CblasRowMajor,
                 CblasNoTrans,
                 CblasNoTrans,
                 second_col,
                 second_col,
                 col,
                 1.0,
                 YTX,
                 col,
                 beta_hat,
                 second_col,
                 0.0,
                 YTX_beta_hat,
                 second_col
                 );


      return (Y_total - YTX_beta_hat[0]) / (scan_number - col);
}

void write_out_voxel_level_info_to_file(const string& filename, const vector<double> &input, const vector<int> &dimensions){
    ofstream myfile;
    myfile.open(filename);
    for(int x = 0; x < dimensions[0]; x++){
        for(int y = 0; y < dimensions[1]; y++){
            for(int z = 0; z < dimensions[2]; z++){
                myfile << input[x * dimensions[1] * dimensions[2] + y * dimensions[2] + z] << " ";
            }
            myfile << endl;
        }
        myfile << endl;
    }
    myfile.close();
}

void write_out_voxel_level_info_to_file_1(const string& filename, const double *input, const vector<int> &dimensions){
    ofstream myfile;
    myfile.open(filename);
    for(int x = 0; x < dimensions[0]; x++){
        for(int y = 0; y < dimensions[1]; y++){
            for(int z = 0; z < dimensions[2]; z++){
                myfile << input[x * dimensions[1] * dimensions[2] + y * dimensions[2] + z] << " ";
            }
            myfile << endl;
        }
        myfile << endl;
    }
    myfile.close();
}

/**
 * 10/1/2018
 * New feature introduced for WLS
 * return value: number of subregions
 * will be stored in subregion_number variable.
 */
void read_subregion_file(const string& filename, const vector<int> &dimensions, vector<int> &res, 
                         int& subregion_number, unordered_map<int, int>& subregion_count, const bool transpose_subregion_file){
    int in, counter = 0;
    ifstream myfile;
    myfile.open("./"+filename);
    while(!myfile.eof()){
        myfile >> in;
        counter++;
        subregion_count[in]++;
    }
    if(counter != dimensions[0] * dimensions[1] * dimensions[2]){
        cerr << "Reading subregion file error! Size not conssistent! Actual: " 
             << counter
             << " Reqire: " 
             << dimensions[0] * dimensions[1] * dimensions[2] << endl;

        exit(0);
    }
    myfile.close();

    myfile.open("./"+filename); // Open twice, need optimization in the future

    double* temp = new double[dimensions[1] * dimensions[2]];
    for(int i = 0; i < dimensions[0]; i++){
        for(int j = 0; j < dimensions[1] * dimensions[2]; j++){
            myfile >> temp[j];
        }
        /* transpose each slice */
        if(transpose_subregion_file) mkl_dimatcopy('R', 'T', dimensions[1], dimensions[2], 1.0, temp, dimensions[1], dimensions[2]);

        for(int j = 0; j < dimensions[1] * dimensions[2]; j++){
            res.push_back(temp[j]);
        }

    }
    myfile.close();
    subregion_number = subregion_count.size()-1;
    delete[] temp;
}

/**
 * Compute rho value used in W matrix
 * formula: \hat{\rho} = \frac{\sum_{k=2}^{n}X_kX_{k-1}}{\sum_{k=2}^{n}X_{k-1}^2}
 * 1/17/2020: formula change:
 */
double compute_rho(const double *Y, const double *beta_hat, const double* X, double sigma, int scan_number, int col, int second_col, int K_blk){
    double *epsilon = new double[scan_number];
    double *XT_beta_hat = new double[scan_number * second_col];

    cblas_dgemm(
           CblasRowMajor,
           CblasNoTrans,
           CblasNoTrans,
           scan_number,
           second_col,
           col,
           1.0,
           X,
           col,
           beta_hat,
           second_col,
           0.0,
           XT_beta_hat,
           second_col
           );

    for(int i = 0; i < scan_number; i++) epsilon[i] = (Y[i] - XT_beta_hat[i]) / sigma;

    double rho = 0.0;
    for(int i = K_blk+1; i < scan_number; i++){
      rho += epsilon[i] * epsilon[i-1];
    }
    double rho_denum = 0.0;
    for(int i = K_blk; i < scan_number-1; i++){
      rho_denum += pow(epsilon[i], 2);
    }
    rho_denum += (pow(epsilon[K_blk-1], 2) + pow(epsilon[scan_number-1], 2)) / 2;
    delete[] epsilon;
    delete[] XT_beta_hat;

    return rho / rho_denum;
}

/**
 * Generate AR(1) matrix look like:
 *
 *         (1    ρ   ρ2  ρ3)
 * sigma^2 (ρ    1   ρ   ρ2)
 *         (ρ2   ρ   1   ρ )
 *         (ρ3   ρ2  ρ   1  )
 * 
 * Note from Dec 3 2018: sigma^2 is discarded. And the right
 * part is actually the inverse of the W matrix
 */
void generate_W_matrix(double *res, double rho, int scan_number){
    fill_n(res, scan_number * scan_number, 0);

    // once rho^n < 0.01, we stop
    int n = 1;
    while(abs(pow(rho, n)) > 0.001 && n < scan_number) n++;
    //cout << "Need " << n-1 << " iterations to be less than 0.01" << endl;

    vector<double> v(scan_number, 0);
    for(int i = 0; i < n; i++){
        v[i] = pow(rho, i);
    }
    for(int i = 0; i < scan_number; i++){
        for(int j = i; j < scan_number; j++){
            res[i*scan_number+j] = v[j-i];
            res[j*scan_number+i] = v[j-i];
        }
    }

    //show_matrix(res, scan_number, scan_number);
    // Based on talk on Dec 3 2018, we inverse the result
    // Update Mar 5 2019, no inverse
    // Update Mar 15 2019, inverse again
    calc_inverse(res, scan_number);
}

/*
 * Compute (X'X)^-1
 */
void compute_XTX_inverse(double* res, const double* X, int scan_number, int col){
  cblas_dgemm(
                CblasRowMajor,
                CblasTrans,
                CblasNoTrans,
                col,
                col,
                scan_number,
                1.0,
                X,
                col,
                X,
                col,
                0.0,
                res,
                col
                );
  //show_matrix(res, col, col);
  calc_inverse(res, col);
  //show_matrix(res, col, col);
}

/*
 * Return by reference: c'(X'WX)^-1c
 * This value is used for estimating Var(c * beta_hat)
 * It is also used for estimating theta_1
 */
double compute_cTXTWX_inverse_c(double* XTWX_inverse, double *c, int col, int second_col){
    double* cTXTWX_inverse_c = new double[second_col];
    double* cT_XTWX_inverse =  new double[second_col * col];  // 1 * 3

            /* Calculate c'(X'X)^{-1} for only once */
            cblas_dgemm(
                  CblasRowMajor,
                  CblasTrans,
                  CblasNoTrans,
                  second_col,
                  col,
                  col,
                  1.0,
                  c,
                  second_col,
                  XTWX_inverse,
                  col,
                  0.0,
                  cT_XTWX_inverse,
                  col
                  );
            /* Calculate c'(X'X)^{-1}c for only once */
            cblas_dgemm(
                  CblasRowMajor,
                  CblasNoTrans,
                  CblasNoTrans,
                  second_col,
                  second_col,
                  col,
                  1.0,
                  cT_XTWX_inverse,
                  col,
                  c,
                  second_col,
                  0.0,
                  cTXTWX_inverse_c,
                  second_col
                  );
    double res = cTXTWX_inverse_c[0];

    delete[] cT_XTWX_inverse;
    delete[] cTXTWX_inverse_c;
    return res;
}

/*
 * Return by reference: XTWX_inverse
 * Return by reference: XTWX_inverse_XTW
 */
void compute_XTWX_inverse(const double* W, const double* X, double* XTWX_inverse, int scan_number, int col){
    double *XTW = new double[col * scan_number];

    /* compute X'W using cblas_dgemm which is provided by Intel MKL */
    cblas_dgemm(
                CblasRowMajor,
                CblasTrans,
                CblasNoTrans,
                col,
                scan_number,
                scan_number,
                1.0,
                X,
                col,
                W,
                scan_number,
                0.0,
                XTW,
                scan_number
                );
    /* compute (X'WX)^-1 using cblas_dgemm which is provided by Intel MKL */
    cblas_dgemm(
                CblasRowMajor,
                CblasNoTrans,
                CblasNoTrans,
                col,
                col,
                scan_number,
                1.0,
                XTW,
                scan_number,
                X,
                col,
                0.0,
                XTWX_inverse,
                col
                );
    calc_inverse(XTWX_inverse, col);

    // cleanup
    delete[] XTW;
}


/*
 * Generate all W related matrices
 */
void generate_W_related_matrices(double** W_matrices, double** cTXTWX_inverse_c_matrices, 
                                 const double* X, vector<int>& dimensions, const vector<double>& rhos,
                                 const vector<vector<double> >& response, const vector<bool>& is_within_ROI, 
                                 int scan_number, int col, int second_col, int subregion_number, 
                                 unordered_map<int, int>& subregion_count, const vector<int>& subregion, 
                                 double **C, int num_of_C){

    if(true){ // generate test file for rhos
      write_out_voxel_level_info_to_file("./test_files/" 
                                         + to_string(scan_number) 
                                         +"/rhos.txt", 
                                         rhos, 
                                         dimensions);
    }

    // For testing purpose: output voxel-wise rho value and generate histogram using external tool
    vector<double> rho_vector(subregion_number+1);
    
    /* Reinitialize */
    clear_2d(W_matrices, subregion_number+1, scan_number*scan_number);
    clear_2d(cTXTWX_inverse_c_matrices, subregion_number+1, num_of_C);

    vector<vector<double>> rho_v(subregion_number+1);
    for (int x = 0; x < dimensions[0]; x++) {
      for (int y = 0; y < dimensions[1]; y++) {
        for (int z = 0; z < dimensions[2]; z++) {
          int pos = x * dimensions[1] * dimensions[2] + y * dimensions[2] + z;
          if(is_within_ROI[pos] && subregion[pos] != 0){
            rho_v[subregion[pos]].push_back(rhos[pos]);
          }
        }
      }
    }

    double** XTWX_inverse_matrices = new double* [subregion_number+1];

    cout << "Done!" << endl;
    for(int i = 1; i < subregion_number+1; i++){
      double median = find_median(rho_v[i]);
      cout << "Rho for scan " << scan_number << " region " << i << " is " << median << endl;
      generate_W_matrix(W_matrices[i], median, scan_number);
      XTWX_inverse_matrices[i] =  new double[col*col];
      compute_XTWX_inverse(W_matrices[i], X, XTWX_inverse_matrices[i], scan_number, col);
      for(int j = 0; j < num_of_C; j++){
        cTXTWX_inverse_c_matrices[i][j] = compute_cTXTWX_inverse_c(XTWX_inverse_matrices[i], C[j], col, second_col);
      }
      delete[] XTWX_inverse_matrices[i];
    }

    delete[] XTWX_inverse_matrices;

}

double find_median(vector<double>& input){
  const auto median_it = input.begin() + input.size() / 2;
  nth_element(input.begin(), median_it , input.end());
  double res = *median_it;
  return res;
}

/*
 * Param: matrix X of size row * col
 * Param: matrix Y of size row * second_col
 * Param: matrix result of size col * col, which is passed by by reference
 * Param: matrix XTX_inverse of size col * col, which is passed back by reference 
 * Output: result contains (X'X)^-1(X'Y), which is beta_hat
 * Functionality: it takes in two matrix, X and Y, and compute
 * (X'X)^-1(X'Y) using Intel MKL library
 */
void compute_beta_hat(const double *X, const double *Y, double *result, const double *XTX_inverse, 
                      const int row, const int col, const int second_col) {

    /* keep the matrix of X'Y */
    double *XTY = new double[col * second_col];

    /* compute X'Y */
    cblas_dgemm(
                CblasRowMajor,
                CblasTrans,
                CblasNoTrans,
                col,
                second_col,
                row,
                1.0,
                X,
                col,
                Y,
                second_col,
                0.0,
                XTY,
                second_col
                );
    
    /* compute (X'X)^-1 (X'Y) */
    cblas_dgemm(
                CblasRowMajor,
                CblasNoTrans,
                CblasNoTrans,
                col,
                second_col,
                col,
                1.0,
                XTX_inverse,
                col,
                XTY,
                second_col,
                0.0,
                result,
                second_col
                );
    
    /* remember to delete dynamic memory to avoid memery leak */
    delete[] XTY;
}


/*
 * Param: matrix X of size row * col
 * Param: matrix Y of size row * second_col
 * Param: matrix result of size col * col, which is passed by by reference
 * Param: matrix XTX_inverse of size col * col, which is passed back by reference 
 * Output: result contains (X'X)^-1(X'Y), which is beta_hat
 * Functionality: it takes in two matrix, X and Y, and compute
 * beta_hat_wls = (X'WX)^-1X'WY using Intel MKL library
 * beta_hat is of size col * second_col
 */
void compute_beta_hat_wls(const double *X, const double* Y, const double* XTWX_inverse_XTW, double *res, 
                          int scan_number, int col, int second_col){
    /* compute (X'WX)^-1(X'WY) using cblas_dgemm which is provided by Intel MKL */
    cblas_dgemm(
                CblasRowMajor,
                CblasNoTrans,
                CblasNoTrans,
                col,
                second_col,
                scan_number,
                1.0,
                XTWX_inverse_XTW,
                scan_number,
                Y,
                second_col,
                0.0,
                res,
                second_col
                );

}

/* 
 * param: easy design matrix location
 * param: hard design matrix location
 * param: new design matrix location
 * param: matrix row
 * param: matrix cut point
 * functionality: concatnate the bottom part of the new matrix to
 * first cut_point rows of the old matrix. Then write it to file.
 */
void assemble_new_degign_matrix(string old_path1, string old_path2, string new_path, int row, int col, int cut_point){
    double d;
    ifstream in(old_path1);
    ofstream out(new_path);
    for(int i=0; i<cut_point; i++){
        for(int j=0; j<col; j++){
            in >> d;
            out << d << " ";
        }
        out << endl;
    }
    in.close();
    in.open(old_path2);
    for(int i=0; i<row; i++){
        for(int j=0; j<col; j++){
            if(i<cut_point){
                in >> d;
            }
            else{
                in >> d;
                out << d << " ";
            }
        }
        if(i >= cut_point)
            out << endl;
    }
    in.close();
    out.close();
}

