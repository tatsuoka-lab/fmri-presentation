/*
 * This code is customized by Weicong and Curtis to show a module to
 * handle the weight lifting part and computes the
 * formula 3 in paper "Dynamic adjustment of stimuli in real time functional
 * magnetic resonance imaging"
 */
#include <iostream>
#include <fstream>
#include <memory>
#include <algorithm>
#include <mkl.h>
#include <cstdlib>
#include <cmath>
#include <ctype.h>
#include <string>
#include <string.h>
#include <vector>
#include <stdio.h>
#include <cilk/cilk.h>
#include <cilk/reducer_opadd.h>
#include <cilk/reducer_ostream.h>
#include <chrono> // to calculate running time
#include <sys/stat.h>
#include <unordered_map> 
using namespace std;

#ifndef numerical_h
#define numerical_h

void clear_2d(double **mat, int row, int col);

void show_matrix(double *mat, int row, int col);

void show_response(vector<vector<double>> response, vector<int> dimensions, int file_num, int Z, int X, int Y);

void init_mat(double *mat, int row, int col, string filepath);

void calc_inverse(double *matrix, int n);

double compute_SPRT( double *beta_hat, int col, double *C, double thetaZero, double thetaOne, double var_cBeta_hat);

vector<int> dimension_parser(const string &s);

vector<double> token_ints(string line);

void read_scan(vector<vector<double> > &response, string file_path, vector<int> &dimensions, int scan_number);

vector<vector<double> > read_all_scans(string file_path_y, vector<int> &dimensions);

vector<double> stop_boundary(double alpha, double beta);

bool check_file_existence (const string& name);

vector<double> estimate_theta1(double** beta_hat, const vector<vector<double>>& response, const vector<int>& dimensions, double *c, int scan_number,
                              int col, int second_col, double Z, double** W_matrices, double* cTXTWX_inverse_c_vector, 
                              const vector<bool>& is_within_ROI, const vector<int>& subregion, 
                              vector<double> &sigma_hat_square_vector, bool write_out_test_file);double compute_rho(const double *Y, const double* beta, const double* X, double sigma, int scan_number, int col, int second_col, int K_blk);

double estimate_sigma_hat_square(const double* X, const double* Y, const double* beta_hat, int scan_number, int col, int second_col);

double compute_sigma_hat_square_old(const double *X, const double *Y, double* YTX, const double* beta_hat, double *YTX_beta_hat, 
                                int scan_number, int col, int second_col);


void write_out_voxel_level_info_to_file(const string& filename, const vector<double> &input, const vector<int> &dimensions);

void write_out_voxel_level_info_to_file_1(const string& filename, const double *input, const vector<int> &dimensions);

void read_subregion_file(const string& filename, const vector<int> &dimensions, vector<int> &res, 
                         int& subregion_number, unordered_map<int, int>& subregion_count, const bool transpose_subregion_file);

double compute_rho(const double *Y, const double *beta_hat, const double* X, double sigma, int scan_number, int col, int second_col, int K_blk);

void generate_W_matrix(double *res, double rho, int scan_number);

void compute_XTX_inverse(double* res, const double* X, int scan_number, int col);

void compute_XTWX_inverse(const double* W, const double* X, double* XTWX_inverse, int scan_number, int col);

double compute_cTXTWX_inverse_c(double* XTWX_inverse, double *c, int col, int second_col);

void generate_W_related_matrices(double** W_matrices, double** cTXTWX_inverse_c_matrices, 
                                 const double* X, vector<int>& dimensions, const vector<double>& rhos,
                                 const vector<vector<double> >& response, const vector<bool>& is_within_ROI, 
                                 int scan_number, int col, int second_col, int subregion_number, 
                                 unordered_map<int, int>& subregion_count, const vector<int>& subregion, 
                                 double **C, int num_of_C);

double find_median(const vector<double>& input);

void compute_beta_hat(const double *X, const double *Y, double *result, const double *XTX_inverse, 
                      const int row, const int col, const int second_col);

void compute_beta_hat_wls(const double *X, const double* Y, const double* XTWX_inverse_XTW, double *res,
						  int scan_number, int col, int second_col);

void assemble_new_degign_matrix(string old_path1, string old_path2, string new_path, int row, int col, int cut_point);

double find_median(vector<double>& input);

double testing_Z_score_impact_by_rho(int pos, double sigma_hat, double* c, double*** BETA_HAT, double* X, 
                                     int scan_number, int col, int second_col, double rho);


#endif
