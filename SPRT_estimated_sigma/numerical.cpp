/*
 * This code is customized by Weicong and Curtis to show a module to
 * handle the weight lifting part and computes the
 * formula 3 in paper "Dynamic adjustment of stimuli in real time functional
 * magnetic resonance imaging"
 */

#include "numerical.h"
using namespace std;

int SCAN_SIZE;
int DIMENSION_SIZE;

/*
 * Clear 2D array
 */
void clear_2d(double **mat, int row, int col) {
    for(int i=0; i<row; i++) fill_n(mat[i], col, 0.0);
}

/**
 * Param: matrix
 * Param: row dimension
 * Param: col dimension
 * Functionality: takes in an matrix and output each slot.
 */
void show_matrix(const double *mat, int row, int col) {
    //cout.precision(17);
    for (int i = 0; i < row; i++) {
        for (int j = 0; j < col; j++) {
            cout << fixed << mat[i * col + j] << " ";
        }
        cout << endl;
    }
    cout << endl;
}

/**
  * Param: response vector which has the dimension SCAN_SIZE * [VOXEL_SIZE_1 * VOXEL_SIZE_2 * VOXEL_SIZE_2]
  * Param: file_num
  * Param: slice number
  * Param: Row number
  * Param: Col number
  * Functionality: takes in a nested vector and output each slot for select files;
  */
void show_response(vector<vector<double>> response, vector<int> dimensions, int file_num, int Z, int X, int Y){
    cout << "File number: " 
         << file_num
         << endl 
         << "Slice number: " 
         << Z << endl 
         << "Voxel location: [" 
         << X 
         << ", " 
         << Y << "]." 
         << "Value stored is :" 
         << response[file_num-1][Z * (dimensions[1] + 1) * (dimensions[2] + 1) + X * (dimensions[2] + 1) + Y]
         <<endl;
}

/*
 * Param: matrix
 * Param: row dimension
 * Param: col dimension
 * Functionality: takes in an matrix and initiate each slot with the
 * values obtained from file, as specified by file path.
 */
void init_mat(double *mat, int row, int col, string filepath) {
    ifstream data (filepath.c_str(), ios::in);
    
    if (!data) {
        cout << "File " << filepath << " could not be opened." << endl;
        exit(1);
    }
    
    for(int i = 0; i < row; i++) {
        for(int j = 0; j < col; j++) {
            data >> mat[i*col+j];
        }
    }
    
    data.close();
}

/*
 * Computes the inverse of a symmetric (Hermitian) positive-definite 
 * matrix using the Cholesky factorization
 */


void calc_inverse(double *matrix, int n){
    int info;

    /* before computing the inversion of the matrix, we need first to factor the matrix */
    dpotrf("U", &n, matrix, &n, &info);

    if(info != 0){
        cerr << "Cholesky factorization failed. Error code " << info << ". Exiting..." << endl;
        exit(0);
    }
    
    dpotri("U", &n, matrix, &n, &info);

    if(info != 0){
        cerr << "Computing inverse of matrix failed. Error code " << info <<". Exiting..." << endl;
        exit(0);
    }

    for(int i=0; i<n; i++){
        for(int j=0; j<n; j++){
            matrix[i*n+j] = matrix[j*n+i];
        }
    }
}


/* 
 * LU factorize and then compute matrix inverse using dgetri
 * Slower than Cholesky and possess some issue
 */
void calc_inverse_dgetri(double *matrix, int n){

    int* ipiv = new int[n];
    int info;
    
    // before computing the inversion of the matrix, we need first to factor the
    // matrix
    dgetrf(&n, &n, matrix, &n, ipiv, &info);

    if(info != 0){
        cerr << "LU factorization failed. Exiting..." << endl;
        exit(0);
    }
    
    double _tmp;

    /*
     * If lwork = -1, then a workspace query is assumed; 
     * the routine only calculates the optimal size of 
     * the work array, returns this value as the first entry 
     * of the work array, and no error message related to lwork 
     * is issued by xerbla.
     */
    int _lwork = -1;
    dgetri(&n, matrix, &n, ipiv, &_tmp, &_lwork, &info);
    _lwork = (int)_tmp;

    if(info != 0){
        cerr << "Calculating optimal size of the work array fails! Exiting..." << endl;
        exit(0);
    }
    
    double *work = new double[_lwork];
    dgetri(&n, matrix, &n, ipiv, work, &_lwork, &info);

    if(info != 0){
        cerr << "Calculating inverse of matrix fails! Exiting..." << endl;
        exit(0);
    }
}

/*
 * Output: SPRT result
 * Functioality: this function is to handle the weight lifting part and computes
 * the formula 3 in paper "Dynamic adjustment of stimuli in real time functional
 * magnetic resonance imaging"
 * Formula: {(c*beta_hat-theta_0)'* Var(c*beta_hat)^-1 * (c*beta_hat-theta_0) 
 *          - (c*beta_hat-theta_1)'* Var(c*beta_hat)^-1 * (c*beta_hat-theta_1)} / 2
 */
double compute_SPRT(
                double *beta_hat, int col, double *c, double thetaZero, double thetaOne, double var_cBeta_hat
                ) {
    double result = 0;
    double CB = 0; // store c * beta_hat
    for (int i = 0; i < col ; i++) {
        CB += beta_hat[i] * c[i];
    }
    return ( pow(CB - thetaZero, 2) - pow(CB - thetaOne, 2) ) / (2 * var_cBeta_hat);
}

/**
 * Param: a string in the header of each response file, e.g. in bold113.txt
 *   it is (36, 128, 128)
 *   or a string in the header of design matrix, e.g. (320, 10)
 * Output: the X-Y-Z dimension values of response file, e.g. 36, 128, 128
 *   or the row-column values of the design matrix
 * Functionality: this method takes in a string composed of dimension parameters
 *   and output three integers indicating the value of each dimension
 */
vector<int> dimension_parser(const string &s) {
    int num = 0;
    vector<int> dimensions;
    bool lastIsDigit = false;
    for (int i = 0; i < s.size(); i++) {
        if (isdigit(s[i])) {
            num *= 10;
            num += s[i] - '0';
            lastIsDigit = true;
        } else {
            if (lastIsDigit) {
                dimensions.push_back(num);
                num = 0;
                lastIsDigit = false;
            }
        }
    }
    //cout << dimensions[0] << " " << dimensions[1] << endl;
    return dimensions;
}


/**
 * This method wrote by Yi is actually incorrect. For example, say a string "1, 2, 3", 
 * it will actually parse it as numbers 1, 12, 123 instead of 1, 2, 3.
 */

/*
 * Param: a string of line containing numbers segmented by ','
 * Output: a vector of integers parsed by ','
 * Functionality: this method takes in a string, say "1,2,3" and parse it as
 * numbers separated by ',' and then return the numbers to the vector */
 
//vector<double> parse_voxel_value(string line) {
//    vector<double> ret;
//    double temp = 0;
//    for (int i = 0; i < line.size(); i++) {
//        if (isdigit(line[i])) {
//            temp *= 10;
//            temp += line[i] - '0';
//        } else {
//            ret.push_back(temp);
//        }
//    }
//    ret.push_back(temp);
//    return ret;
//}


/**
 * Param: a string of line containing numbers segmented by ','
 * Output: a vector of integers parsed by ','
 * Functionality: this method takes in a string, say "1,2,3" and parse it as
 *   numbers separated by ',' and then return the numbers to the vector
 */
vector<double> parse_voxel_value(string line) {
    istringstream ss (line);
    vector <double> ret;
    while(ss){
        string s; 
        if(!getline(ss, s, ',')) break;
        ret.push_back(stod(s));
    }
    return ret;
}

/**
 * Param: a nested vector of double type which contains the response matrix
 * Param: a string identifying the bold text file location
 * Param: a vector of int which contains the dimension information
 * Param: a counter of int to specify the latest scan number when doing real time analysis
 * Output: none
 * Functionality: For time-series issue, the single response vector is nested into 2-dimensional array.
 *    Each file is a row response and SCAN_SIZE files means SCAN_SIZE rows of response.
 */
void read_scan(vector<vector<double> > &response, string file_path, vector<int> &dimensions, int scan_number) {
    file_path+="bold"+to_string(scan_number)+".txt";
    ifstream myfile (file_path);
    string line = "";
    
    //cout << "Reading file " << file_path << endl;

    if (myfile.is_open()) {
        getline(myfile, line);
        
        /* Z - X - Y */
        dimensions = dimension_parser(line);
        
        /* get the maximum element as the base value, here (VOXEL_SIZE_2 + 1) */
        vector<double> ret (dimensions[0] * dimensions[1] * dimensions[2], 0);
        int X = 0, Y = 0, Z = -1;
        while(!myfile.eof()) {
            getline(myfile, line);
            
            /* meanning if we can find a match of word "slice" in the string */
            if (strstr(line.c_str(), "Slice") != NULL) {
                /* this is a line like "#new slice" */
                Z++;
                /* reset x axis to zero */
                X = 0;
            } else {
                /* this is a line containing numbers */
                /* reset Y to 0 */
                Y = 0;
                vector<double> line_tokens = parse_voxel_value(line);
                
                for (; Y < line_tokens.size(); Y++) {
                    // cout << "Z is" << Z << " X is " << X << " Y is " << Y <<  endl;
                    ret[Z * dimensions[1] * dimensions[2] + X * dimensions[2] + Y] = line_tokens[Y];
                }
                
                X++;
            }
        }
        /* a 2-D row here means a time-stamp */
        response.push_back(ret);
    } else {
        cout << "Can't find file " << file_path << endl;
        throw "File open error at reading matrix.";
    }
    
    myfile.close();
}

/**
 * Param: string of file path
 * Param: vector of integer which contains dimensions
 * Output: a 2-D array encoding response values
 * Functionality: this method simply runs on top of method read_scan and
 *   enumerate all possible file names and read them, store the values in the
 *   2-D response vector.
 *   Each file corresponds to the response for a certain time slot and makes up
 *   a row in 2-D array.
 */
vector<vector<double> > read_all_scans(string file_path_y, vector<int> &dimensions) {
    vector<vector<double> > response;
    for (int i = 1; i <= 100; i++) {
        string temp = file_path_y + "bold";
        
        //if (i < 10) {
        //    temp += "000";
        //    temp.append(1, i + '0');
        //} else if (i >= 10 && i <= 99) {
        //    temp += "00";
        //    string temp_second = "";
        //    int k = i;
        //    while (k > 0) {
        //        temp_second.append(1, k % 10 + '0');
        //        k /= 10;
        //    }
        //    reverse(temp_second.begin(), temp_second.end());
        //    temp += temp_second;
        //} else { // i >= 100
        //    //temp += "0";
        //    string temp_second = "";
        //    int k = i;
        //    while (k > 0) {
        //        temp_second.append(1, k % 10 + '0');
        //        k /= 10;
        //    }
        //    reverse(temp_second.begin(), temp_second.end());
        //    temp += temp_second;
        //}

        temp += to_string(i);
        temp += ".txt";
        // once the file names is generated, read contents from them
        read_scan(response, temp, dimensions, 0);
    }
    return response;
}

/**
 * Param: double -- alpha, double -- beta
 * Output: the stopping rule's boundaries [A, B]
 * Functionality: this method takes in two parameters, alpha and bate, and
 *   computes the stopping rule's boundary values A and B
 */
vector<double> stop_boundary(double alpha, double beta) {
    double A = log((1 - beta) / alpha), B = log(beta / (1 - alpha));
    vector<double> boundary;
    boundary.push_back(A);
    boundary.push_back(B);
    return boundary;
}

/*
 * Functionality: check for file existence for real-time analysis
 */
bool check_file_existence (const string& name) {
  struct stat buffer;   
  return (stat (name.c_str(), &buffer) == 0); 
}

void estimate_theta1 (vector<vector<double> >& res, const vector<vector<double> >& response, const vector<int>& dimensions, const double *X, double **C, int num_of_C, int scan_number, int col, int second_col, double Z, const vector<bool> & is_within_ROI){
    int size = dimensions[0] * dimensions[1] * dimensions[2];
    double *beta_hat = new double[col * second_col];  // 3 * 1
    double *XTX_inverse = new double[col * col];
    double* cTXTX_inverse_c = new double[num_of_C];
    double **var_cT_beta_hat = new double* [num_of_C];
    for(int i=0; i<num_of_C; i++) var_cT_beta_hat[i] =  new double[size]();
    
    compute_XTX_inverse(XTX_inverse, X, scan_number, col); // compute (X'X)^-1 for only once, this is required for computing beta_hat
    for(int i = 0; i < num_of_C; i++) cTXTX_inverse_c[i] = compute_cTXTX_inverse_c(XTX_inverse, C[i], scan_number, col, second_col);

    /* for each point in X-Y-Z space, compute the response array Y */
    for (int x = 0; x < dimensions[0]; x++) {
      for (int y = 0; y < dimensions[1]; y++) {
        for (int z = 0; z < dimensions[2]; z++) {
          if(is_within_ROI[x * dimensions[1] * dimensions[2] + y * dimensions[2] + z]){
            int pos = x * dimensions[1] * dimensions[2] + y * dimensions[2] + z;
            double *Y = new double[scan_number * second_col]; // Initialize the response matrix
            for (int scan = 0; scan < scan_number; scan++) {
              Y[scan] = response[scan][x * dimensions[1] * dimensions[2] + y * dimensions[2] + z];
            }

            compute_beta_hat(X, Y, beta_hat, XTX_inverse, scan_number, col, second_col);

            /* 
             * start computing var(c'beta_hat)
             */
            double sigma_hat_square = estimate_sigma_hat_square(X, Y, beta_hat, scan_number, col, second_col);
            for(int i=0; i<num_of_C; i++){
              var_cT_beta_hat[i][pos] = sigma_hat_square * cTXTX_inverse_c[i];
              res[i][pos] = Z * sqrt(var_cT_beta_hat[i][pos]);
            }
            delete[] Y;
          }
        }
      }
    }
    delete[] beta_hat;
    delete[] XTX_inverse;
    for(int i=0; i<num_of_C; i++) delete[] var_cT_beta_hat[i];
    delete[] cTXTX_inverse_c;
    delete[] var_cT_beta_hat;
}

/*
 * Z = c'beta / sqrt(var(c'beta))
 */
void compute_Z_score(double** Z_score, double** C, const double* beta_hat, double** var_cT_beta_hat, int num_of_C, int pos, int col, int second_col){
  double* cT_beta_hat = new double[second_col];
  for(int i = 0; i < num_of_C; i++){
    /* compute c'beta_hat */
    cblas_dgemm(
          CblasRowMajor,
          CblasTrans,
          CblasNoTrans,
          second_col,
          second_col,
          col,
          1.0,
          C[i],
          second_col,
          beta_hat,
          second_col,
          0.0,
          cT_beta_hat,
          second_col
          );
    Z_score[i][pos] = cT_beta_hat[0] / sqrt(var_cT_beta_hat[i][pos]);
  }
  delete[] cT_beta_hat;
}

void write_out_voxel_level_info_to_file(const string& filename, const vector<double> &input, const vector<int> &dimensions){
    ofstream myfile;
    myfile.open(filename);
    for(int x = 0; x < dimensions[0]; x++){
        for(int y = 0; y < dimensions[1]; y++){
            for(int z = 0; z < dimensions[2]; z++){
                myfile << input[x * dimensions[1] * dimensions[2] + y * dimensions[2] + z] << " ";
            }
            myfile << endl;
        }
        myfile << endl;
    }
    myfile.close();
}

void write_out_voxel_level_info_to_file_1(const string& filename, const double *input, const vector<int> &dimensions){
    ofstream myfile;
    myfile.open(filename);
    for(int x = 0; x < dimensions[0]; x++){
        for(int y = 0; y < dimensions[1]; y++){
            for(int z = 0; z < dimensions[2]; z++){
                myfile << input[x * dimensions[1] * dimensions[2] + y * dimensions[2] + z] << " ";
            }
            myfile << endl;
        }
        myfile << endl;
    }
    myfile.close();
}

/*
 * Param: matrix X of size row * col
 * Param: matrix Y of size row * second_col
 * Param: matrix result of size col * col, which is passed by by reference
 * Param: matrix XTX_inverse of size col * col, which is passed back by reference 
 * Output: result contains (X'X)^-1(X'Y), which is beta_hat
 * Functionality: it takes in two matrix, X and Y, and compute
 * (X'X)^-1(X'Y) using Intel MKL library
 */
void compute_beta_hat(const double *X, const double *Y, double *result, const double *XTX_inverse, 
                      const int row, const int col, const int second_col) {

    /* keep the matrix of X'Y */
    double *XTY = new double[col * second_col];

    /* compute X'Y */
    cblas_dgemm(
                CblasRowMajor,
                CblasTrans,
                CblasNoTrans,
                col,
                second_col,
                row,
                1.0,
                X,
                col,
                Y,
                second_col,
                0.0,
                XTY,
                second_col
                );
    
    /* compute (X'X)^-1 (X'Y) */
    cblas_dgemm(
                CblasRowMajor,
                CblasNoTrans,
                CblasNoTrans,
                col,
                second_col,
                col,
                1.0,
                XTX_inverse,
                col,
                XTY,
                second_col,
                0.0,
                result,
                second_col
                );
    
    /* remember to delete dynamic memory to avoid memery leak */
    delete[] XTY;
}

/* 
 * param: easy design matrix location
 * param: hard design matrix location
 * param: new design matrix location
 * param: matrix row
 * param: matrix cut point
 * functionality: concatnate the bottom part of the new matrix to
 * first cut_point rows of the old matrix. Then write it to file.
 */
void assemble_new_degign_matrix(string old_path1, string old_path2, string new_path, int row, int col, int cut_point){
    double d;
    ifstream in(old_path1);
    ofstream out(new_path);
    for(int i=0; i<cut_point; i++){
        for(int j=0; j<col; j++){
            in >> d;
            out << d << " ";
        }
        out << endl;
    }
    in.close();
    in.open(old_path2);
    for(int i=0; i<row; i++){
        for(int j=0; j<col; j++){
            if(i<cut_point){
                in >> d;
            }
            else{
                in >> d;
                out << d << " ";
            }
        }
        if(i >= cut_point)
            out << endl;
    }
    in.close();
    out.close();
}

/*
 * Compute (X'X)^-1
 */
void compute_XTX_inverse(double* res, const double* X, int scan_number, int col){
  cblas_dgemm(
                CblasRowMajor,
                CblasTrans,
                CblasNoTrans,
                col,
                col,
                scan_number,
                1.0,
                X,
                col,
                X,
                col,
                0.0,
                res,
                col
                );
  //show_matrix(res, col, col);
  calc_inverse(res, col);
  //show_matrix(res, col, col);
}

double compute_cTXTX_inverse_c(double *XTX_inverse, double *c, int scan_number, int col, int second_col){
  double* cTXTX_inverse = new double[col * second_col];
  double* cTXTX_inverse_c = new double[second_col * second_col];

  cblas_dgemm(
                CblasRowMajor,
                CblasTrans,
                CblasNoTrans,
                second_col,
                col,
                col,
                1.0,
                c,
                second_col,
                XTX_inverse,
                col,
                0.0,
                cTXTX_inverse,
                col
                );

  cblas_dgemm(
                CblasRowMajor,
                CblasNoTrans,
                CblasNoTrans,
                second_col,
                second_col,
                col,
                1.0,
                cTXTX_inverse,
                col,
                c,
                second_col,
                0.0,
                cTXTX_inverse_c,
                second_col
                );
  double res = cTXTX_inverse_c[0];
  delete[] cTXTX_inverse;
  delete[] cTXTX_inverse_c;
  return res;
}

/*
 * estimate sigma_hat^2
 * sigma_hat ^2 = sum{r_i^2} / (# of scans - # of parameters)
 * r_i = Y_i - X * beta_hat
 */
double estimate_sigma_hat_square(const double* X, const double* Y, const double* beta_hat, int scan_number, int col, int second_col){

    double * XT_beta_hat = new double[scan_number * second_col];

    cblas_dgemm(
           CblasRowMajor,
           CblasNoTrans,
           CblasNoTrans,
           scan_number,
           second_col,
           col,
           1.0,
           X,
           col,
           beta_hat,
           second_col,
           0.0,
           XT_beta_hat,
           second_col
           );

    double res = 0;
    for(int i = 0; i < scan_number; i++) 
        res += pow(Y[i] - XT_beta_hat[i], 2);
    res /= (scan_number-col);
    return res;
}