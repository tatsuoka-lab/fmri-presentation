import numpy as np
import matplotlib.pyplot as plt
import sys

name = str(sys.argv[1])
arr = np.loadtxt("a.txt")
arr = arr.flatten()
plt.hist(arr, bins='auto')
plt.title(name)
plt.show()